Facade Pattern
=======

Description
----------
Facade as the name suggests means the face of the building. The people walking past the road can only see this glass face of the building. They do not know anything about it, the wiring, the pipes and other complexities. The face hides all the complexities of the building and displays a friendly face.

Facade pattern hides the complexities of the system and provides an interface to the client using which the client can access the system. This type of design pattern comes under structural pattern as this pattern adds an interface to existing system to hide its complexities.

The Facade design pattern is often used when a system is very complex or difficult to understand.

![Facade_Structure.jpg](http://4.bp.blogspot.com/-W6xz6EFoXxA/VKVX38_E7cI/AAAAAAAACT0/t6AgrjrF4xI/s1600/facade%2Bpattern%2Bin%2BJava.png)


Example source code
----------

In this example, the flag will be printed using facade pattern. Horizontal and Vertical line are the complex parts.

Facade class is DrawerFacade. The client will use DrawerFacade to draw the flag.

```
#!java

public interface Line {
	void drawLine();
}

public class Horizontal implements Line {
	@Override
	public void drawLine() {
		System.out.print("--------");
	}
}

public class Vertical implements Line {
	@Override
	public void drawLine() {
		for(int i = 0; i<5 ;i++ ) {
			System.out.println("|");			
		}
	}
}
/* Facade */
public class DrawerFacade {
	private Line horizontal;
	private Line vertical;
	public DrawerFacade() {
		horizontal = new Horizontal();
		vertical = new Vertical();
	}
	public void drawFlag(){
		for( int i = 0; i<5 ; i++){
			horizontal.drawLine();
			horizontal.drawLine();
			System.out.println();
		}
		vertical.drawLine();
	}
}
/* Client */
public class Client {
	public static void main(String[] args){
		DrawerFacade drawer = new DrawerFacade();
		drawer.drawFlag();
	}
}
```

Exercise
---------
Use Facade pattern to calculate the price of 3 cookie, 11 Donut, and 7 chocolate.

Then, print out the infomation with total price.

```
#!Java

public interface SaleItem {
	double getPrice();
	String toString();
}

public class Donut implements SaleItem {
	@Override
	public double getPrice() {
		return 7;
	}
	@Override
	public String toString() {
		return "Donut";
	}
}

public class Cookie implements SaleItem{
	@Override
	public double getPrice() {
		return 5;
	}
	@Override
	public String toString() {
		return "Cookie";
	}
}

public class Chocolate implements SaleItem {
	@Override
	public double getPrice() {
		return 15;
	}
	@Override
	public String toString() {
		return "Chocolate";
	}
}

public class StoreFacade {
	/* Put your code here */
	public void calculateTotal(int numCookie, int numDonut, int numChocolate){
	/* Put your code here */
	}
}

public class customer {
	public static void main(String[] args){
		StoreFacade store = new StoreFacade();
		store.calculateTotal(3, 11, 7);
	}
}
```